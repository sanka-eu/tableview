package com.example.task_1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun showimage (imageview: ImageView, x: Int) {
            when(x) {
                1 -> imageview.setBackgroundResource(R.drawable.blatnoy)
                2 -> imageview.setBackgroundResource(R.drawable.hitriy)
                3 -> imageview.setBackgroundResource(R.drawable.miliy)
                4 -> imageview.setBackgroundResource(R.drawable.podozritelniy)
                5 -> imageview.setBackgroundResource(R.drawable.zadumchiviy)
                else -> imageview.setBackgroundResource(R.drawable.ic_launcher_background)
            }
        }

        button.setOnClickListener() {
            var x: Int = (1..5).random()
            when(x) {
                1 -> button.setBackgroundColor(getResources().getColor(R.color.teal_700))
                2 -> button.setBackgroundColor(getResources().getColor(R.color.purple_500))
                else -> button.setBackgroundColor(getResources().getColor(R.color.teal_200))
            }
            showimage(image, x)
        }
    }
}
